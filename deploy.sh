#!/bin/bash
set -x



echo ""
echo "........................................"
echo "Installation of application"
echo "........................................"
echo "Today's date: `date`"
echo "........................................"
echo ""
sudo apt-get update
echo "........................................"
echo "apt update completed........................................"
sudo apt-get install default-jdk -y
echo "installing java........................................"

java -version
echo "installing tomcat 8........................................"

sudo apt-get install tomcat8 -y
sudo apt-get install tomcat8-docs tomcat8-examples tomcat8-admin -y

sudo cp -r /usr/share/tomcat8-admin/* /var/lib/tomcat8/webapps/ -v
echo "changing port from 8080 to 4000........................................"

sudo sed -i 's/8080/4000/g' /var/lib/tomcat8/conf/server.xml
echo "set up a user in tomcat........................................"

CONTENT="<role rolename=\"manager-script\"/>\n<user username=\"tomcat\" password=\"password\" roles=\"manager-script\"/>"		
C=$(echo $CONTENT | sed 's/\//\\\//g')
sed "/<\/tomcat-users>/ s/.*/${C}\n&/" /var/lib/tomcat8/conf/tomcat-users.xml >> file
mv /var/lib/tomcat8/conf/tomcat-users.xml /var/lib/tomcat8/conf/tomcat-users.xml.bck
mv file /var/lib/tomcat8/conf/tomcat-users.xml 
echo "add memory to jvm........................................"

sed -i 's/JAVA_OPTS/#/g' /etc/default/tomcat8
echo "JAVA_OPTS=\"-Djava.security.egd=file:/dev/./urandom -Djava.awt.headless=true -Xmx512m -XX:MaxPermSize=512m -XX:+UseConcMarkSweepGC\"" >> /etc/default/tomcat8
echo "restart tomcat........................................"

sudo systemctl restart tomcat8
echo "check status........................................"
echo this script was written by team excellence use with permission

sudo systemctl status tomcat8

exit

