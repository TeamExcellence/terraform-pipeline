

variable region {
    type = string
  default = "us-east-2"
}
variable volume_size {}
variable instance_type{}

variable application {}
variable "environment" {}
#variable "java_home" {
#  type = string
 # default = "/opt/java/jdk1.8.0_251"
#}
############## tags
variable os {
  type        = string
  default = "Ubuntu"
}
variable launched_by {
  type        = string
  default = "USER"
}
############## end tags