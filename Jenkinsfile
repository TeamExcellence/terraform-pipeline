pipeline {
    agent {
      node {
        label "master"
      } 
    }

    parameters {
        string(name: 'AppName', defaultValue: 'Enter App Name', description: 'Name of application', )
        choice(choices: ['master', 'dev', 'qa', 'uat', 'prod-a', 'prod-b'], description: 'Select lifecycle to Deploy', name: 'Branch')
        choice(choices: ['t2.micro', 't2.small', 't2.large', 't2.xlarge'], description: 'Select Instance Size', name: 'InstanceSize')
        choice(choices: ['8', '10', '20', '30'], description: 'Select Volume Size', name: 'VolumeSize')
        booleanParam(name: 'autoApprove', defaultValue: false, description: 'Automatically run apply after generating plan?')
    }


     environment {
        AWS_ACCESS_KEY_ID     = credentials('AWS_ACCESS_KEY_ID')
        AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
        TF_VAR_instance_type = "${params.InstanceSize}"
        TF_VAR_environment = "${params.Branch}"
        TF_VAR_application = "${params.AppName}"
        TF_VAR_volume_size = "${params.VolumeSize}"

    }
// 

    stages {
      stage('checkout') {
        steps {
            echo "Pulling changes from the branch ${params.Branch}"
            git credentialsId: 'b6bcc87a-125e-4aed-982f-505bf6b60568', url: 'https://TeamExcellence@bitbucket.org/TeamExcellence/terraform-pipeline.git' , branch: "${params.Branch}"
        }
      }

        stage('terraform plan') {
            steps {
                sh "pwd ; terraform init -input=true"
                sh "terraform plan -input=true -out tfplan"
                sh 'terraform show -no-color tfplan > tfplan.txt'
}
            }
        
        stage('terraform apply approval') {
           when {
               not {
                   equals expected: true, actual: params.autoApprove
               }
           }

           steps {
               script {
                    def plan = readFile 'tfplan.txt'
                    input message: "Do you want to apply the plan?",
                    parameters: [text(name: 'Plan', description: 'Please review the plan', defaultValue: plan)]
               }
           }
       }

        stage('terraform apply') {
            steps {
                sh "terraform apply -input=true tfplan"
            }
        }
        
        stage('terraform destroy approval') {
            steps {
                input 'Run terraform destroy?'
            }
        }
        stage('terraform destroy') {
            steps {
                sh 'terraform destroy -force'
            }
        }
    }

  }